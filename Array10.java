/**
* 
* Practica EntornosDeDesarrollo
* 
*/

import java.util.Scanner;

public class Array10
{
	
	public static void main (String[] args) 
	{
	
	double cadena[] =new double[10];
	double media=0.0;
	
	Scanner sc = new Scanner (System.in);
	
	System.out.println("Programa práctica Entornos de Desarrollo. Media de array de 10 enteros");
	
	for (int i=0; i<10; i++){
		System.out.println("Introduce la posición: "+i);
		cadena[i]=sc.nextDouble();
		media=((media*i)+cadena[i])/(i+1);
		System.out.println("La media es: "+media);
		}
	}
}
